# Kitsat magnetometer 3D visualization script
# Tessa Nikander, July 28th 2022
# dependencies: matplotlib
 
import csv
from mpl_toolkits import mplot3d
import numpy as np
import matplotlib.pyplot as plt
fig = plt.figure()
ax = plt.axes(projection='3d')
zdata = []
xdata = []
ydata = []
with open('data.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    for row in csv_reader:
        if 'IMU' in row:
            xdata.append(row[2])
            ydata.append(row[3])
            zdata.append(row[4])
        
xdata=np.array(xdata,dtype=float)
ydata=np.array(ydata,dtype=float)
zdata=np.array(zdata,dtype=float)
 
ax.scatter3D(xdata, ydata, zdata, c=zdata, cmap='Greens');
plt.show()