## Q: Is my Kitsat hardware version supported? 🤔

## A: Yes! We support all versions that have been distributed since the beginning of time.

If you can't find your versions, don't hesitate to contact support@kitsat.fi

## Firmware upgrade procedure

1) Disassemble Kitsat.

* PL (payload) board (green) is on top.
* EPS (electrical power system) board (red, or green if strato version) is in the middle.
* OBC (on-board computer) board (blue) is on bottom.

2) Check your PL configuration. The PL version can be read from the topmost PCB (green) of your satellite stack. Choose the firmware directory accordingly.

3) Check your OBC configuration. If you have a white NUCLEO board on your OBC, you must choose NUCLEO update.


### NUCLEO update

Switch on your Kitsat. Plug it to your PC via USB. Open file manager. Drag and drop the downloaded `kitsat.bin` binary file to the
Kitsat file system (NOD_F401RE) root.

Wait until the LEDs have finished flashing. Kitsat will restart and the update file is automatically removed from Kitsat.
Your system is now up-to-date.

### SD card update

Switch off your Kitsat. Detach the micro SD card from Kitsat OBC. Plug it to your PC. Copy and paste the downloaded `kitsat-update.bin` file to the SD card root.

Deplug your micro SD card from your PC and reattach it to Kitsat OBC. Switch on Kitsat.

Wait until the LEDs have finished flashing. Kitsat will restart and the update file is automatically removed from Kitsat.
Your system is now up-to-date.

## Problems?

Please contact us at support@kitsat.fi .