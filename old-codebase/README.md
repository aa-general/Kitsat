## Access the old codebase

A very early version of the Kitsat codebase is publicly available.
The code is written for the first flight model in 2018.

It can be accessed here:
https://os.mbed.com/users/samulinyman/code/DiySAT/

