import os
import setuptools

with open ('README.md', 'r') as f:
    long_description = f.read()

setuptools.setup(
        name='kitsat_python',
        version='1.1',
        author='Tuomas Simula',
        author_email='tuomas@simu.la',
        description='CLI and python library for communicating with the Kitsat educational satellite',
        long_description=long_description,
        long_description_content_type='text/markdown',
        packages=setuptools.find_packages(),
        include_package_data=True,
        install_requires=[
            'pyserial>=3.0',
            'blessed>=1.16.0',
        ],
        classifiers=[
            'Programming Language :: Python :: 3',
            'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
            'Operating System :: OS Independent',
        ],
        python_requires='>=3.4',

        scripts=['kitsat/bin/kitsat_cli'],
)
