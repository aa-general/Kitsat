#!/usr/bin/env python 
#
# Script to light the LEDs of Kitsat
#
# This file is part of kitsat-python. https://github.com/netnspace/Kitsat-Python-Library
# (C) 2020 Tuomas Simula <tuomas@simu.la>
#
# SPDX-License-Identifier:   GPL-3.0-or-later

from kitsat import Modem
from time import sleep

# Connect automatically to satellite or gs
m = Modem()
m.connect_auto()

networkid = 1       # If connected through radio, satellite network id is required
amt_of_cycles = 10  # How many times the loop should run

# See if connected to groundstation or satellite
# If groundstation, set network id of satellite
m.write('ping_local')
if m.read()[4] == '0':
    m.write('gs_set_network {}'.format(networkid))

# Array to determine which LED is which color
lights_array = [[1, 4, 2], [2, 1, 4], [4, 2, 1]]

# Loop to light the LEDs
for i in range(amt_of_cycles):
    for i in range(3):
        for j in range(3):
            m.write('led_on {} {}'.format(j+1, lights_array[j][i]))
            sleep(0.03)
        sleep(1)

# Turn LEDs off
m.write('led_off 0')
sleep(0.1)

# Disconnect from satellite
m.disconnect()
