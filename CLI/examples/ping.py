#!/usr/bin/env python
#
# Simple script that connects to a satellite on port '/dev/ttyACM0'
# sends the 'ping' command to it, and prints the reseived data
#
# This file is part of kitsat-python. https://github.com/netnspace/Kitsat-Python-Library
# (C) 2020 Tuomas Simula <tuomas@simu.la>
#
# SPDX-License-Identifier:  GPL-3.0-or-later

from kitsat import Modem

# Connect automatically to satellite or gs
m = Modem()
m.connect_auto()

networkid = 1   # If connected through radio, satellite network id is required

# See if connected to groundstation or satellite
# If groundstation, set network id of satellite
m.write('ping_local')
if m.read()[4] == '0':
    m.write('gs_set_network {}'.format(networkid))

# Write the ping command and print the read output
m.write('ping')
print(m.read())

# Disconnect from satellite
m.disconnect()
