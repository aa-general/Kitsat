#!/usr/bin/env python
#
# Program to stream temperature data from the BMP280 sensor
# and save it in a CSV file for analysing
#
# This file is part of kitsat-python. https://github.com/netnspace/Kitsat-Python-Library
# (C) 2020 Tuomas Simula <tuomas@simu.la>
#
# SPDX-License-Identifier:  GPL-3.0-or-later

from kitsat import Modem
from time import sleep, time
import csv

# Initialise modem object and variables
m = Modem()
datapoints = 60 # Amt of measurements to take
timestep = 1    # Time between each measurement

# Connect automatically to satellite or gs
m.connect_auto()

# See if connected to groundstation or satellite
# If groundstation, set network id of satellite
m.write('ping_local')
if m.read()[4] == '0':
    m.write('gs_set_network {}'.format(networkid))

# Collect temperature data and save it to a csv file
with open('tmp_data.csv', 'w') as csvfile:
    tmpwriter = csv.writer(csvfile)
    t_start = time()
    for i in range(datapoints):
        m.write('bmp_get_temp')		# Command to measure temperature
        t = time() - t_start		# Time from start of program
        sleep(timestep)				# Sleep for the set timestep
        data = m.read()				# Read data
        
        # Write the data to the csv file and print it
        tmpwriter.writerow([t, data[4]])
        print("{:.2f}: {}".format(t, data[4]))

# Disconnect from satellite
m.disconnect()
