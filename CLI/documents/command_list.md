## Kitsat command list

### CLI commands

These commands control the command line interface. They can be used with the CLI, but are not recognized by the satellite itself.

#### beep\_on\_connect

 * Change the boolean value of whether the satellite should beep on succesful connection. As default, the value is true

#### connect

 * __Param type:__ str
 * __Param explanation:__ str port
 * Connect manually to port

#### connect\_auto

 * Connect automatically to a satellite or groundstation

#### disconnect

 * Disconnect from satellite or groundstation

#### exit

 * Exit program

#### help

 * __Param type:__ (str)
 * __Param explanation:__ optional str command for which to print help message
 * Print help message

#### list\_commands

 * List all commands

#### list\_ports

 * List all serial ports

#### port\_in\_use

 * Which port is currently in use

#### set\_timeout

 * __Param type:__ float
 * __Param explanation:__ float timeout
 * Set timeout in seconds for serial connection

#### cam\_download\_pic

 * __Param type:__ int
 * __Param explanation:__ int img\_index
 * Download image with index i from satellite over radio
 
### Satellite commands

These commands are used to control the Kitsat satellite or groundstation. They can be used in the CLI, or using the library.

#### beep

 * __Param type:__ int
 * __Param explanation:__ int n
 * Beep n times

#### beep\_stop

 * Stop buzzer

#### morse

 * __Param type:__ str
 * __Param explanation:__ str message
 * Morse with buzzer

#### WhoAmI

 * Gets version number

#### ping\_local

 * Test the UART

#### ping

 * Test the connection

#### reset

 * Reset satellite

#### get\_version

 * Get FW version number

#### get\_status

 * Check if all subsystems work nominally

#### cam\_list

 * __Param type:__ int
 * __Param explanation:__ int n
 * List n latest images

#### cam\_num\_blocks

 * __Param type:__ int
 * __Param explanation:__ int img\_index
 * Number of blocks in image chunks

#### cam\_get\_blocks

 * __Param type:__ int|int
 * __Param explanation:__ int block\_index| int img\_index
 * Get blocks with index of image i (defaults to latest)

#### cam\_take\_pic

 * Takes picture

#### cam\_stream

 * Streams an image without saving

#### cam\_get\_latest

 * Get latest image name

#### gps\_get\_time

 * GPS time

#### gps\_get\_location

 * GPS location

#### gps\_get\_velocity

 * GPS velocity

#### gps\_get\_altitude

 * GPS altitude

#### gps\_fix

 * GPS fix status

#### gps\_get\_all

 * GPS all data in binary format

#### bmp\_get\_temp

 * BMP temperature

#### bmp\_get\_pres

 * BMP pressure

#### imu\_get\_mag

 * IMU magnetometer

#### imu\_get\_gyr

 * IMU gyroscope

#### imu\_get\_acc

 * IMU accelerometer

#### imu\_calibrate

 * Calibrate IMU

#### imu\_get\_all

 * IMU all measurements

#### rf\_get\_temp

 * RF temperature

#### rf\_get\_rssi

 * Satellite rssi

#### led\_off

 * __Param type:__ int
 * __Param explanation:__ int led
 * Stop LED i blinking

#### led\_on

 * __Param type:__ int|int
 * __Param explanation:__ "int led, chr color"
 * Lit LED i with color w

#### reset\_local

 * Reset groundstation

#### gs\_set\_nodeid

 * __Param type:__ int
 * __Param explanation:__ int id
 * Set node id

#### gs\_get\_nodeid

 * Get node id

#### gs\_get\_RSSI

 * Get groundstation RSSI

#### gs\_set\_network

 * __Param type:__ int
 * __Param explanation:__ int id
 * Set network

#### reset\_gs

 * Reset groundstation

#### eps\_get\_voltage

 * Get battery voltage

#### eps\_measure

 * Get ADC measurements

#### eps\_get\_sp\_voltage

 * Get EPS x current

#### eps\_get\_sp\_current

 * Get EPS y current

#### sd\_card\_connected

 * Is the SD card in the satellite?

#### rbf\_connected

 * Is the RBF connected?

#### eps\_charging

 * Is the battery charging?
