# kitsat\_python API

## Classes

### class kitsat.Modem

#### __init__(port = None, timeout = 1, beep\_on\_connect = False)

Parameters:
 * port (str) - Device name or None
 * timeout (float) - Read and write timeout value
 * beep\_on\_connect (bool) - Whether satellite beeps on connect

If port is not None, the modem tries to immediately connect to the specified port.

If port is None or the connection fails, a call to connect() is required.

Port is a device name: depending on operating system. e.g. /dev/ttyACM0 on GNU/Linux or COM3 on Windows.

#### port

(str) Name of serial port currently in use or None

#### timeout

(float) Read and write timeout value

#### beep\_on\_connect

(bool) Whether satellite beeps on connect

#### ports

(list) List of serial ports available, must be updatet manually with list\_ports()

#### is\_connected

(bool) Whether a satellite or groundstation is connected

#### connect(port)

Parameters:
 * port (str or int) - Port to connect to, either index of port in Modem.ports or name of port

Connects to satellite or groundstation on specified port. Raises serial.serialutil.SerialException if connection fails

#### connect\_auto()

Connects automatically to first satellite or groundstation found

#### disconnect()

Disconnects from satellite or groundstation

#### write(cmd, timeout = None)

Parameters:
 * cmd (str) - Command to send to satellite
 * timeout (float) - Write timeout, if None, defaults to Modem object's timeout value

Writes command to satellite

#### writeraw(cmd, timeout = None)

Parameters:
 * cmd (bytearr) - Manually parsed command to send to satellite
 * timeout (float) - Write timeout, if None, defaults to Modem object's timeout value

Writes manually parsed command to satellite

#### read(timeout = None)

Parameters:
 * timeout (float) - Read timeout , if None, defaults to Modem object's timeout value

Returns:
 * (list) Packet received from satellite

Reads one received packet from satellite. Returns a list of data:
[orig\_id, cmd\_id, data\_len, timestamp, data, fnv]

#### in\_waiting()

Returns:
 * (int) Amount of packets waiting to be read

Return the amount of packets waiting to be read from the satellite

#### list\_ports()

Returns:
 * (list) List of serial ports available

Return list of device names of the serial ports available to connect to

## Helper functions

### kitsat.cmd\_parser

#### parse(cmd)

Parameters:
 * cmd (str) - Command to parse
 
Returns:
 * (bytearr) Command parsed into package that can be sent to satellite using the writeraw() method
 
Parse command string into package that can be sent to satellite
 
### kitsat.packet\_parser

#### parse(pkg)

Parameters:
 * pkg (bytearr) - Package to parse
 
Returns:
 * (list) A list containing parsed data from the satellite
 
Return list of data parsed from the satellite in the form
```python
\[(int) orig\_id, (int) cmd\_id, (int) data\_len, (int) timestamp, (str) data, (int) fnv\]
```

### kitsat.math\_utils

#### fnv(bytear)

Parameters:
 * bytear (bytearr) - Packet to calculate the FNV for
 
Returns:
 * (int) - A FNV checksum
  
Calculate the FNV checksum of a packet

#### check\_fnv(bytear)

Parameters:
 * bytear (bytearr) - Packet to check the FNV for
 
Returns:
 * (bool) Whether packet matches FNV checksum
 
 Calculate FNV for packet and compare it to FNV that came with the packet

