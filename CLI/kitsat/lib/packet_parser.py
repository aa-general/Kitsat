#!/usr/bin/env python
#
# Functions for parsing packets received from satellite into human-readable form
#
# This file is part of kitsat-python. https://github.com/netnspace/Kitsat-Python-Library
# (C) 2020 Tuomas Simula <tuomas@simu.la>
#
#SPDX-License-Identifier:   GPL-3.0-or-later

from . import math_utils

def parse(pkg):
    """Function for extracting data from packet and logging it"""

    timestamp_int = int.from_bytes(pkg[3:7], byteorder='little')

    orig_int = pkg[0]
    cmd_id_int = pkg[1]
    data_len_int = pkg[2]
    fnv_int = int.from_bytes(pkg[-4:], byteorder='little')

    data = pkg[7:7+data_len_int]
    data_str = parse_bytedata(orig_int, cmd_id_int, data)
    data_arr = [orig_int, cmd_id_int, data_len_int, timestamp_int, data_str, fnv_int]

    # Check that packet matches checksum
    if math_utils.check_fnv(pkg):
        return data_arr
    else:
        return "package corrupted"


def parse_bytedata(orig, cmd_id, data):
    """Function for parsing received data into human-readable form"""

    data = data.decode('utf_8')

    return(data)
